# JsRender Hello World with Node.js

A tiny project to demo how to use JsRender with express-powered Node.js apps.

## Usage

```
$ git clone https://gitlab.com/cwchen/jsrender-hello-node.git
$ cd jsrender-hello-node
$ npm install
$ npm start
```

## Copyright

2018, Michael Chen; the project is licensed under Apache 2.0.
