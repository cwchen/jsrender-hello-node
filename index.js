// Load libraries
var express = require("express");
var jsrender = require("jsrender");

// Create an express app.
var app = express();

// Set JsRender as the template language of this app.
app.engine('html', jsrender.__express); // Set JsRender as template engine for .html files
app.set('view engine', 'html'); 
app.set('views', __dirname + '/templates'); // Folder location for JsRender templates for Express

// Write the callback for root path "/".
app.get("/", function (req, res) {
    res.render('index', {greeting: "Hello World"});
});


// Start the app.
app.listen(8080, function () {
  console.log('Example app listening on port 8080!');
});
